import setuptools

parser_name = 'base_sql_parser'

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name=f"{parser_name}_parser",
    version="0.0.1",
    author="Alon Barad",
    description="A shell commands to SQL db parser.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/riftool/parsers/Shell_Commands_Parser",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
)
