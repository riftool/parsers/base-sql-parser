# Riftool Parser Template

### OS environment variables
AMQP_URL for Rabbit MQ URL
DB_CONNECTION_STRING for SQL database connection string
TABLE_NAME the SQL detention table name

### Product exaple
```json
{
    "product_id": "affea072-6b29-4f68-8bbd-8defb9991c3f",
    "command_id": "af4415c5-ef85-44fa-ad69-79fab87177cd",
    "utc_execution_time": "2020-07-12 12:31:30",
    "tool_id": "1234567890",
    "raw_data": "Command output"
}
```

### Use example
```python
from base_sql_parser import SQLParser

parser = SQLParser(exchange_name='products', product_type='ShellCommands')
parser.run()
```