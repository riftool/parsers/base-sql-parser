from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from base_sql_parser.database.table import base, Table


class DBManager:

    def __init__(self, connection_string):
        self.engine = create_engine(connection_string)
        base.metadata.create_all(self.engine)

    def add_record(self, product_id, command_id, tool_id, command_output):
        session = sessionmaker(self.engine)()
        try:
            record = dict(product_id=product_id, command_id=command_id, tool_id=tool_id, command_output=command_output)
            command = Table(**record)
            session.add(command)
            session.commit()
        finally:
            session.bind.dispose()
