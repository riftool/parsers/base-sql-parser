import datetime
import os

from sqlalchemy import Column, String, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base

base = declarative_base()
TABLE_NAME = os.environ.get('TABLE_NAME')


class Table(base):
    __tablename__ = TABLE_NAME
    product_id = Column(UUID(as_uuid=True), primary_key=True, unique=True, nullable=False)
    command_id = Column(UUID(as_uuid=True), nullable=False)
    tool_id = Column(String, nullable=False)
    command_output = Column(String)
    utc_execution_time = Column(DateTime)
    utc_insertion_time = Column(DateTime, default=datetime.datetime.utcnow)
