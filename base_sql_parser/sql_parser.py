import json
import os

from base_parser.base_parser import BaseParser

from base_sql_parser.database.db_manager import DBManager

DB_CONNECTION_STRING = os.environ.get('DB_CONNECTION_STRING')


class SQLParser(BaseParser):

    def _callback(self, channel, method, properties, body) -> None:
        self.logger.info('Received product.', extra={'properties': properties})
        self.send_product(self.parse_product(body))

    def parse_product(self, product_buffer):
        return json.loads(product_buffer)

    def send_product(self, parsed_product) -> None:
        db = DBManager(connection_string=DB_CONNECTION_STRING)
        db.add_record(**parsed_product)
